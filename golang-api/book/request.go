package book

import "encoding/json"

type BookRequest struct {
	Title string  		  `json:"title" binding:"required"`
	Price json.Number     `json:"price" binding:"required"`	
	Genre string  		  `json:"genre" binding:"required"`
	Rating json.Number  		  `json:"rating" binding:"required"`
	Discount json.Number  		  `json:"discount" binding:"required"`	
}