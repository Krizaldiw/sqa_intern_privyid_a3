package main

import (
	//"fmt"
	"golang-api/book"
	"golang-api/handler"
	"log"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
  	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("DB connection Error")
	}
	//Migration Data
	db.AutoMigrate(&book.Book{})

	//bookRepository := book.NewRepository(db)

	bookRepository := book.NewFileRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	router := gin.Default()

	v1 := router.Group("/v1")

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/hello", bookHandler.HelloHandler)
	// v1.GET("/books/:id/:title", bookHandler.BookHandler)
	// v1.GET("/query", bookHandler.QueryHandler)

	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBooks)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run(":8000") 
}
	/* bookRequest := book.BookRequest{
		Title: "$1000 Startup",
		Price: "100000",
	}
	
	bookService.Create(bookRequest) */

	//Create Data

	/* book := book.Book{}
	book.Title = "Kekalahan Barcelona"
	book.Price = 55000
	book.Discount = 5
	book.Rating = 4
	book.Description = "Ini adalah buku yang sangat bagus dari Khalif Rizaldi"

	err = db.Create(&book).Error
	if err != nil {
		fmt.Println("====================")
		fmt.Println("Error creating book record")
		fmt.Println("====================")
	} */

	/* var book book.Book

	//Find Data	
	err = db.Debug().Where("id = ?", 1).First(&book).Error
	if err != nil {
		fmt.Println("====================")
		fmt.Println("Error creating book record")
		fmt.Println("====================")
	}

	//Delete Data
	err = db.Delete(&book).Error
	if err != nil {
		fmt.Println("====================")
		fmt.Println("Error creating book record")
		fmt.Println("====================")
	} */

	/* book.Title = "Barcelona di Bantai King Munchen (Limited Edition)"
	err = db.Save(&book).Error
	if err != nil {
		fmt.Println("====================")
		fmt.Println("Error updating book record")
		fmt.Println("====================")
	} */

	/* for _, b := range books {
		fmt.Println("Title", b.Title)
		fmt.Println("book object %v", b)
	} */


// func rootHandler(c*gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"name": "Khalif Rizaldi",
// 		"profile": "A Software Quality Assurance Engineer at PrivyID",
// 	})
// }

// func helloHandler(c*gin.Context) {
// 	c.JSON(http.StatusOK, gin.H{
// 		"title": "Hello Kamu",
// 		"subtitle": "Belajar Golang bareng Agung Setiawan",
// 	})
// }

// func bookHandler(c*gin.Context) {
// 		id := c.Param("id")
// 		title := c.Param("title")
// 		c.JSON(http.StatusOK, gin.H{"id": id, "title": title})
// }

// func queryHandler(c*gin.Context) {
// 	title := c.Param("title")
// 	price := c.Param("price")
// 	c.JSON(http.StatusOK, gin.H{"id": title, "price": price})
// }



